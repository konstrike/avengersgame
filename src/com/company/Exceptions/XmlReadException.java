package com.company.Exceptions;

public class XmlReadException extends RuntimeException {
    public XmlReadException(String message){
        super(message);
    }
}
