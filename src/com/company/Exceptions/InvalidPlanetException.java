package com.company.Exceptions;

public class InvalidPlanetException extends RuntimeException{
    public InvalidPlanetException(String message){
        super(message);
    }
}
