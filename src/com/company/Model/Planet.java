package com.company.Model;

public class Planet {
    private int id;
    private String name;
    private String description;
    private Modifiers modifiers;

    public Planet(int id, String name, String description, Modifiers modifiers) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.modifiers = modifiers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Modifiers getModifiers() {
        return modifiers;
    }

    public void setModifiers(Modifiers modifiers) {
        this.modifiers = modifiers;
    }

    @Override
    public String toString() {
        return "Planet:\n" +
                "-id= " + id +
                "\n-name= " + name +
                "\n-description= " + description +
                "\n-modifiers= " + modifiers.toString() +
                '\n';
    }
}
