package com.company.Model;

public class AvengCharacter {
    private int id;
    private String name;
    private String description;
    private long attack;
    private long health;
    private boolean isVillain;

    public AvengCharacter(int id, String name, String description, long attack, long health, boolean isVillain) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.attack = attack;
        this.health = health;
        this.isVillain = isVillain;
    }

    public AvengCharacter() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getAttack() {
        return attack;
    }

    public void setAttack(long attack) {
        this.attack = attack;
    }

    public long getHealth() {
        return health;
    }

    public void setHealth(long health) {
        this.health = health;
    }

    public boolean isVillain() {
        return isVillain;
    }

    public void setVillain(boolean villain) {
        isVillain = villain;
    }

    public void addPlanetModifier(long attack,long health)
    {
        this.attack+=attack;
        this.health+=health;
    }

    public void takeDamage(long damage)
    {
        this.health-=damage;
    }

    @Override
    public String toString() {
        return "Character\n" +
                "id= " + id +
                "\nname= " + name +
                "\ndescription= " + description +
                "\nattack= " + attack +
                "\nhealth= " + health +
                "\nisVillain= " + isVillain +
                "\n";
    }
}
