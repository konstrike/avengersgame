package com.company.Model;

public class Modifiers {
    private long heroAttackModifier;
    private long heroHealthModifier;
    private long villainAttackModifier;
    private long villainHealthModifier;

    public Modifiers(long heroAttackModifier, long heroHealthModifier, long villainAttackModifier, long villainHealthModifier) {
        this.heroAttackModifier = heroAttackModifier;
        this.heroHealthModifier = heroHealthModifier;
        this.villainAttackModifier = villainAttackModifier;
        this.villainHealthModifier = villainHealthModifier;
    }

    public long getHeroAttackModifier() {
        return heroAttackModifier;
    }

    public void setHeroAttackModifier(long heroAttackModifier) {
        this.heroAttackModifier = heroAttackModifier;
    }

    public long getHeroHealthModifier() {
        return heroHealthModifier;
    }

    public void setHeroHealthModifier(long heroHealthModifier) {
        this.heroHealthModifier = heroHealthModifier;
    }

    public long getVillainAttackModifier() {
        return villainAttackModifier;
    }

    public void setVillainAttackModifier(long villainAttackModifier) {
        this.villainAttackModifier = villainAttackModifier;
    }

    public long getVillainHealthModifier() {
        return villainHealthModifier;
    }

    public void setVillainHealthModifier(long villainHealthModifier) {
        this.villainHealthModifier = villainHealthModifier;
    }

    @Override
    public String toString() {
        return "Modifiers{" +
                "heroAttackModifier=" + heroAttackModifier +
                ", heroHealthModifier=" + heroHealthModifier +
                ", villainAttackModifier=" + villainAttackModifier +
                ", villainHealthModifier=" + villainHealthModifier +
                '}';
    }
}
