package com.company.Model.Factories;

import com.company.Model.Modifiers;
import com.company.Model.Planet;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PlanetFactory implements IFactory<Planet> {
    /**
     * Build and return a Planet based on the information in the Node
     *
     * input: Node
     * output: Planet
     */
    @Override
    public Planet getEntityFromXML(Node e) {
        Element planetElement=(Element) e;
        int id=Integer.parseInt(planetElement.getElementsByTagName("id").item(0).getTextContent());
        String name=planetElement.getElementsByTagName("name").item(0).getTextContent().toString();
        String description=planetElement.getElementsByTagName("description").item(0).getTextContent().toString();
        NodeList modifiersNode=planetElement.getElementsByTagName("modifiers");
        Node modNode=modifiersNode.item(0);
        Element modifierElement=(Element) modNode;
        int heroAttackModifier=Integer.parseInt(modifierElement.getElementsByTagName("heroAttackModifier").item(0).getTextContent());
        int heroHealthModifier=Integer.parseInt(modifierElement.getElementsByTagName("heroHealthModifier").item(0).getTextContent());
        int villainAttackModifier=Integer.parseInt(modifierElement.getElementsByTagName("villainAttackModifier").item(0).getTextContent());
        int villainHealthModifier=Integer.parseInt(modifierElement.getElementsByTagName("villainHealthModifier").item(0).getTextContent());
        Modifiers mod=new Modifiers(heroAttackModifier,heroHealthModifier,villainAttackModifier,villainHealthModifier);
        return new Planet(id,name,description,mod);

    }
}
