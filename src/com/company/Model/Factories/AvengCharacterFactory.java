package com.company.Model.Factories;

import com.company.Model.AvengCharacter;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class AvengCharacterFactory implements IFactory<AvengCharacter> {
    /**
     * Build and return a AvengCharacter based on the information in the Node
     *
     * input: Node
     * output: AvengCharacter
     */
    @Override
    public AvengCharacter getEntityFromXML(Node e) {
        Element characterElement=(Element) e;
        int id=Integer.parseInt(characterElement.getElementsByTagName("id").item(0).getTextContent());
        String name=characterElement.getElementsByTagName("name").item(0).getTextContent().toString();
        String description=characterElement.getElementsByTagName("description").item(0).getTextContent().toString();
        Long attack=Long.parseLong(characterElement.getElementsByTagName("attack").item(0).getTextContent());
        Long health=Long.parseLong(characterElement.getElementsByTagName("health").item(0).getTextContent());
        Boolean isVilain=Boolean.parseBoolean(characterElement.getElementsByTagName("isVillain").item(0).getTextContent());
        AvengCharacter newCharacter=new AvengCharacter(id,name,description,attack,health,isVilain);
        return newCharacter;
    }
}
