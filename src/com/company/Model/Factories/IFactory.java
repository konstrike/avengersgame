package com.company.Model.Factories;

import org.w3c.dom.Node;

public interface IFactory<E>{
    E getEntityFromXML(Node e);
}
