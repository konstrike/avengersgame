package com.company.Controller;

import com.company.Exceptions.InvalidPlanetException;
import com.company.Model.Planet;
import com.company.Repository.Repo;

import java.util.List;
import java.util.NoSuchElementException;

public class PlanetController {
    private Repo<Planet> repository;

    public PlanetController(Repo<Planet> repo)
    {
        this.repository=repo;
    }

    /**
     * Return all vilains
     *
     * input: -
     * output: List<Planet>
     */
    public List<Planet> getPlanets()
    {
        return repository.getAll();
    }

    /**
     * Return the Planet with that id
     *
     * input: int
     * output: Planet
     * Exception: InvalidPlanetException if the id doesn't exist
     */
    public Planet getPlanetById(int id)
    {
        List<Planet> planets=repository.getAll();
        try {
            return planets.stream().filter(i -> i.getId() == id).findFirst().get();
        }catch (NoSuchElementException e)
        {
            throw new InvalidPlanetException("Invalid id!");
        }
    }
}
