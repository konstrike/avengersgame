package com.company.Controller;

import com.company.Exceptions.InvalidCharacterException;
import com.company.Model.AvengCharacter;
import com.company.Model.Planet;
import com.company.Repository.Repo;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.stream.Collectors;

public class CharacterController {
    private Repo<AvengCharacter> repository;

    public CharacterController(Repo<AvengCharacter> repo) {
        this.repository = repo;
    }

    /**
     * Return all vilains
     *
     * input: -
     * output: List<AvengCharacter>
     */
    public List<AvengCharacter> getVillains() {
        List<AvengCharacter> characters = repository.getAll();
        return characters.stream().filter(c -> c.isVillain()).collect(Collectors.toList());
    }

    /**
     * Return all heroes
     *
     * input: -
     * output: List<AvengCharacter>
     */
    public List<AvengCharacter> getHeroes() {
        List<AvengCharacter> characters = repository.getAll();
        return characters.stream().filter(c -> !c.isVillain()).collect(Collectors.toList());
    }

    /**
     * Return the AvengCharacter with that id
     *
     * input: int
     * output: AvengCharacter
     */
    private AvengCharacter getCharacterById(int id) {
        List<AvengCharacter> characters = repository.getAll();
        return characters.stream().filter(c -> c.getId() == id).findFirst().get();

    }

    /**
     * Return the villain with that id
     *
     * input: int
     * output: AvengCharacter
     * Exception: InvalidCharacterException if the id doesn't exist or it isn't the id of a villain
     * */
    public AvengCharacter getVillainById(int id) {
        try {
            AvengCharacter ac = getCharacterById(id);
            if (ac.isVillain())
                return ac;
            else
                throw new InvalidCharacterException("This id is not for a villain!");
        } catch (NoSuchElementException e) {
            throw new InvalidCharacterException("Invalid id!");
        }
    }

    /**
     * Return the hero with that id
     *
     * input: int
     * output: AvengCharacter
     * Exception: InvalidCharacterException if the id doesn't exist or it isn't the id of a hero
     * */
    public AvengCharacter getHeroById(int id) {
        try {
            AvengCharacter ac = getCharacterById(id);
            if (ac.isVillain() == false)
                return ac;
            else
                throw new InvalidCharacterException("This id is not for a hero!");
        } catch (NoSuchElementException e) {
            throw new InvalidCharacterException("Invalid id!");
        }
    }

    /**
     * Apply PlanetModifier to the heroes and the villain
     *
     * input: List<AvengCharacter>, AvengCharacter , Planet
     * output: -
     */
    public void addPlanetModifier(List<AvengCharacter> hero, AvengCharacter villain, Planet planet) {
        long heroattack = planet.getModifiers().getHeroAttackModifier();
        long herohealth = planet.getModifiers().getHeroHealthModifier();
        for (AvengCharacter h : hero)
            h.addPlanetModifier(heroattack, herohealth);

        long villainattack = planet.getModifiers().getVillainAttackModifier();
        long villainhealth = planet.getModifiers().getVillainHealthModifier();
        villain.addPlanetModifier(villainattack, villainhealth);

    }

    /**
     * input: int ,List<AvengCharacter>
     * output: -
     * Exception: InvalidCharacterException if there is already a character with that id in the team
     */
    public void check(int id, List<AvengCharacter> heroes) {
        for (AvengCharacter hero : heroes) {
            if (id == hero.getId())
                throw new InvalidCharacterException("Hero already in team!");
        }
    }

    /**
     * Return a random attack between 60% - 100% of the initial power
     *
     * input: int
     * output: long
     */
    public long generateAttack(long attack) {
        int randomAttackRange = new Random().nextInt(40) + 60;

        return (attack * randomAttackRange) / 100;
    }

    /**
     * Return true if there is at least on AvengCharacter with more than 0 Hp, otherwise return false
     *
     * input: List<AvengCharacter>
     * output: boolean
     */
    public boolean checkHp(List<AvengCharacter> heroes) {
        for (AvengCharacter ac : heroes) {
            if (ac.getHealth() > 0)
                return true;
        }
        return false;
    }

    /**
     * Return a random from the heroes team
     *
     * input: List<AvengCharacter>
     * output: AvengCharacter
     */
    public AvengCharacter getRandomHero(List<AvengCharacter> heroes) {
        int randomHero = new Random().nextInt(heroes.size());
        while (heroes.get(randomHero).getHealth() <= 0)
            randomHero = new Random().nextInt(heroes.size());
        return heroes.get(randomHero);
    }
}
