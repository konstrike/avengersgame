package com.company.UI;

import com.company.Controller.CharacterController;
import com.company.Controller.PlanetController;
import com.company.Exceptions.InvalidCharacterException;
import com.company.Exceptions.InvalidPlanetException;
import com.company.Model.AvengCharacter;
import com.company.Model.Planet;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class BaseUI {
    private CharacterController characterController;
    private PlanetController planetController;
    private List<AvengCharacter> heroes = new LinkedList<>();
    private AvengCharacter villain;
    private Planet planet;

    public BaseUI(CharacterController charController, PlanetController planetController) {
        this.characterController = charController;
        this.planetController = planetController;
    }

    /**
     * Display all Heroes
     */
    private void displayHeroes() {
        characterController.getHeroes().stream().forEach(System.out::println);
    }

    /**
     * Display all Villains
     */
    private void displayVillains() {
        characterController.getVillains().stream().forEach(System.out::println);
    }

    /**
     * Display all Planets
     */
    private void displayPlanets() {
        planetController.getPlanets().stream().forEach(System.out::println);
    }

    /**
     * Chose a Hero
     * @return a AvengCharacter or Null base on the input
     */
    private AvengCharacter choseHero() {
        System.out.println("List of Heroes:");
        displayHeroes();
        System.out.println("Choose the id of the hero:");
        Scanner sc = new Scanner(System.in);
        try {
            int chose = Integer.parseInt(sc.nextLine());
            characterController.check(chose, heroes);
            return characterController.getHeroById(chose);
        } catch (InvalidCharacterException ice) {
            System.out.println(ice.getMessage());
        } catch (Exception e) {
            System.out.println("Invalid id format! Must be a number!");
        }
        return null;
    }

    /**
     * Chose a Villain
     * @return a AvengCharacter or Null base on the input
     */
    private AvengCharacter choseVillain() {
        System.out.println("List of Villains:");
        displayVillains();
        System.out.println("Choose the id of the villain:");
        Scanner sc = new Scanner(System.in);
        try {
            int chose = Integer.parseInt(sc.nextLine());
            return characterController.getVillainById(chose);
        } catch (InvalidCharacterException ice) {
            System.out.println(ice.getMessage());
        } catch (Exception e) {
            System.out.println("Invalid id format! Must be a number!");
        }
        return null;
    }

    /**
     * Chose a Planet
     * @return a Planet or Null base on the input
     */
    private Planet chosePlanet() {
        System.out.println("List of Planets:");
        displayPlanets();
        System.out.println("Choose the id of the planet");
        Scanner sc = new Scanner(System.in);
        try {
            int chose = Integer.parseInt(sc.nextLine());
            return planetController.getPlanetById(chose);
        } catch (InvalidPlanetException ipe) {
            System.out.println(ipe.getMessage());
        } catch (Exception e) {
            System.out.println("Invalid id format! Must be a number!");
        }
        return null;
    }

    /**
     * Chose the Planet, the Villain and the Hero/Heroes
     */
    private void choseLoadout() {

        planet = chosePlanet();
        while (planet == null) {
            System.out.println("Try again :)\n");
            planet = chosePlanet();
        }

        villain = choseVillain();
        while (villain == null) {
            System.out.println("Try again :)\n");
            villain = choseVillain();
        }
        int check;
        do {
            check = 0;
            AvengCharacter hero = choseHero();
            while (hero == null) {
                System.out.println("Try again :)\n");
                hero = choseHero();
            }
            heroes.add(hero);
            System.out.println("Do you want another hero in the team?");
            System.out.println("1)Yes\n2)No");
            Scanner sc = new Scanner(System.in);
            try {
                int chose = Integer.parseInt(sc.nextLine());
                if (chose == 1)
                    check = 1;
            } catch (Exception e) {
                System.out.println("Invalid command! This will be a no.");
            }
        } while (check == 1);

    }

    /**
     * Add modifiers of the planets to characters
     */
    private void insertPlanetModifier() {
//        heroes.stream().forEach(System.out::println);
//        System.out.println(villain.toString());
        characterController.addPlanetModifier(heroes, villain, planet);
//        heroes.stream().forEach(System.out::println);
//        System.out.println(villain.toString());
    }

    /**
     * The fight between Hero/Heroes and Villain
     */
    private void fight() {
        while (characterController.checkHp(heroes) && villain.getHealth() > 0) {
//            int villainAttackProc = new Random().nextInt(40) + 60;
//            int heroAttackProc = new Random().nextInt(40) + 60;

            AvengCharacter hero=characterController.getRandomHero(heroes);
            long villainDamage = characterController.generateAttack(villain.getAttack());
            long heroDamage = characterController.generateAttack(hero.getAttack());

            System.out.println(villain.getName() + " hit for " + villainDamage + "!");
            hero.takeDamage(villainDamage);
            System.out.println(hero.getName() + " have " + hero.getHealth() + " health left!");
            if (hero.getHealth() < 0) {
                System.out.println("Hero " + hero.getName() + " has been defeated!");
//                System.out.println("You lost");
//                break;
            }
            if(characterController.checkHp(heroes)==false) {
                System.out.println("Heroes have been defeated!");
                System.out.println("You lost");
                break;
            }

            System.out.println(hero.getName() + " hit for " + heroDamage + "!");
            villain.takeDamage(heroDamage);
            System.out.println(villain.getName() + " have " + villain.getHealth() + " health left!");
            System.out.println("----------");
        }


        if (villain.getHealth() <= 0) {
            System.out.println("Villain " + villain.getName() + " has been defeated!");
            System.out.println("You won");
        }
    }

    public void execute() {
        choseLoadout();
        insertPlanetModifier();
        fight();
    }
}
