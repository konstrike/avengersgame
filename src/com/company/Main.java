package com.company;

import com.company.Controller.CharacterController;
import com.company.Controller.PlanetController;
import com.company.Model.AvengCharacter;
import com.company.Model.Factories.AvengCharacterFactory;
import com.company.Model.Factories.IFactory;
import com.company.Model.Factories.PlanetFactory;
import com.company.Model.Planet;
import com.company.Repository.Repo;
import com.company.Repository.XmlRepository.XmlRepositoryCharacters;
import com.company.UI.BaseUI;

public class Main {

    public static void main(String[] args) {
        IFactory<AvengCharacter> factoryCharacter=new AvengCharacterFactory();
        Repo<AvengCharacter> repo=new XmlRepositoryCharacters<>("E:\\Data files\\Desktop\\CoreBuild Avengers Game\\src\\com\\company\\Repository\\XmlRepository\\characters.xml","character",factoryCharacter);

        IFactory<Planet> factoryPlanet=new PlanetFactory();
        Repo<Planet> repoPlanet=new XmlRepositoryCharacters<>("E:\\Data files\\Desktop\\CoreBuild Avengers Game\\src\\com\\company\\Repository\\XmlRepository\\planets.xml","planet",factoryPlanet);

        CharacterController characterController=new CharacterController(repo);
        PlanetController planetController=new PlanetController(repoPlanet);
        BaseUI ui=new BaseUI(characterController,planetController);
        ui.execute();
    }
}
