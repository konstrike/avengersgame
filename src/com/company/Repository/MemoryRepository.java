package com.company.Repository;

import java.util.LinkedList;
import java.util.List;

public class MemoryRepository<E> implements Repo<E>{

    List<E> elems;

    public MemoryRepository()
    {
        elems =new LinkedList<>();
    }

    @Override
    public void save(E elem) {
        elems.add(elem);
    }

    @Override
    public List<E> getAll() {
        return elems;
    }
}
