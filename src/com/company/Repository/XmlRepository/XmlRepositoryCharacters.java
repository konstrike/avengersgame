package com.company.Repository.XmlRepository;

import com.company.Exceptions.XmlReadException;
import com.company.Model.Factories.IFactory;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import com.company.Repository.Repo;
import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class XmlRepositoryCharacters<E> implements Repo<E> {

    List<E> elems;
    private String file;
    private String entityName;
    private IFactory<E> entityFactory;

    public XmlRepositoryCharacters(String file, String entityName, IFactory<E> entityFactory)
    {
        elems =new LinkedList<>();
        this.file=file;
        this.entityName=entityName;
        this.entityFactory=entityFactory;
        loadInfo();
    }

    private void loadInfo()
    {
        try {
            File xmlFile=new File(file);
            DocumentBuilderFactory factory= DocumentBuilderFactory.newInstance();
            DocumentBuilder builder=factory.newDocumentBuilder();
            Document doc=builder.parse(xmlFile);
            NodeList nodeList=doc.getElementsByTagName(entityName);
            for(int i=0;i<nodeList.getLength();i++)
            {
                Node node=nodeList.item(i);
                if(node.getNodeType()==Node.ELEMENT_NODE)
                {
                    E entity=entityFactory.getEntityFromXML(node);
                    this.save(entity);
                }
            }
        } catch (Exception e) {
            throw new XmlReadException("There was a problem in loading xml format!");
        }
    }

    @Override
    public void save(E elem) {
        elems.add(elem);
    }

    @Override
    public List<E> getAll() {
        return elems;
    }
}
