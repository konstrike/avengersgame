package com.company.Repository;

import java.util.List;

public interface Repo<E> {

    void save(E elem);
    List<E> getAll();
}
